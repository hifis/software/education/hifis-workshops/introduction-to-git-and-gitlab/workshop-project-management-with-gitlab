<!---
SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden - Rossendorf (HZDR) <hifis-info@hzdr.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# Project Management with GitLab

A workshop on managing software projects with GitLab

## Design Considerations

* **Duration:** One full work day (8 hours) in total including 1 hour of breaks
* **Environment:** Primarily designed for online teaching. 
* **Prerequisites:** A browser, access to any _GitLab_ instance, local _git_ installation
* **Previous Knowledge:** Shell and Git, e.g. from an SWC course

## Covered Topics

* Setting up a GitLab-Project
    * Creating the Repository
    * Adding Members
    * Licensing
    * README
    * Contribution Guide
* (Tie-In) using a GitLab Repo from Git
* Introducing a Workflow
    * Create Issues -> Start MRs -> Work on it -> Review -> Merge
* Issues
    * Labels
    * Boards
* Merge Requests
* Issue- and MR-Templates
* Reviews
* Automation with CI

## Project Structure

* Teaching instructions are found in the `episodes` folder
* Copyable material for workshop participants to use in their own project and during the course is found in the [snippets]

## License

Any content of this project defaults to the 
[![Creative Commons License Agreement](https://i.creativecommons.org/l/by/4.0/80x15.png "CC-BY 4.0")](https://creativecommons.org/licenses/by/4.0/)
_Creative Commons Attribution 4.0 International License_ 
unless explicitly stated otherwise.
You can find a full-text version of the license agreement in the [LICENSE](LICENSE) file.

[snippets]: https://gitlab.com/hifis/hifis-workshops/project-management-with-gitlab/-/snippets
