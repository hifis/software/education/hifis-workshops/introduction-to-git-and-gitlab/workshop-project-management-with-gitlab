<!---
SPDX-FileCopyrightText: 2020 HIFIS <hifis-info@hzdr.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# Setting up a new GitLab Project
> **Estimated time:** 30 minutes
>
> ## Learning Goals
> * Create and manage groups
> * Create a new GitLab repository with a README
> * Learn about licensing and contribution guides
> * Sharing the project 
>
> ## Preparation
> * Make sure everyone has a working GitLab account
> * Create an initial group for the workshop
> * Include all participants of the workshop
>   * Everybody needs to be at least **maintainer** to be able to create subgroups later on.
>   * Don't forget instructors and helpers
>
> **Note:** When adding files in this episode, we do not concern ourself with branches, issues and Merge Requests yet.
> It is okay to just directly put the files on _master_.
> Make sure example README, LICENSE and CONTRIBUTING files are prepared.

---

## Forming Groups
 
* GitLab users can form a _group_.
* Groups are useful if people are regularly working together on projects
* Groups can be nested within other groups (subgroups)
* Individuals can be in as many groups as they want
* Projects can be moved between groups (may break links, so use it carefully)

> ### Do Together 
> * Everyone creates a subgroup within the project group
> * Give yourself a fancy group name e.g. _Ingenious Instructors Inc._ :smile:
>    * Instructors: Add each other as group members
>    * Participants: Add Instructors as group members
> * See also: [Gitlab Permissions]

## Creating the Project itself 
* Projects can be either within the space of a single person or a group
  * In the latter case, group members automatically get access rights
  * For collaboration projects in a fixed team (e.g. a research group) creating the project in a group is preferable (increased Bus-factor).

> ### Do Together 
> * Everybody creates a new blank project in the shared group
>    * Do _not_ yet initialize the project with a README
> * Explain the other options to transfer an existing project into GitLab

## Writing a README
* A README is the entry point for everyone who looks at your project.
* It should at least contain information on 
  * What the project is about
  * How to use the project
  * Where to find further information
  * Who and how to contact in case of inquiries
* For software projects the [Standard Readme] would be a good starting point
* For GitLab (and GitHub) the usuual way is to put a file `README.md` into the project's top-level folder
* **Keep in mind:** The README is never final. It may require updates as your project develops.

> ### Do Together
> * Everybody adds a README into their project
>   * An example outline can be found in [the snippets]($2004220)

## Choosing a License
* Your license choice affects what people can do with it
* If you do not choose a license, nobody can use your project in any way
  * Even if you intend this, adding a proprietary license for clarification is strongly recommended
* GitLab expects a license file called `LICENSE` (or `LICENSE.md`) in the project's top-level folder
* A more dilligent approach would be to follow the [reuse specification]
  * Includes a toolset to handle your licenses
  * Helpful when handling multiple licenses
  * In a combined approach the `LICENSE` points to the `LICENSES` folder
* Good licenses for sharing include:
  * [Creative Commons Licenses] for printed material, art, design and websites
  * MIT, GPL or Apache Licenses for Code
  * If in doubt: Ask your legal department, especially if involving 3rd party projects
  * **Note:** If this is a work project, make sure you have the copyright and that you may actually choose the intended license
* More information on licensing can also be found in the [_Bring your own Script…_-Workshop](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials/-/blob/master/episodes/04_add-license.md)

> ### Do Together
> * Add a license to the project in a _reuse_-compliant way
>  * Basically any license will do
>  * Create a `LICENSES` folder
>  * Put your chosen license into that folder
>    * Here is a [list of compatible licenses](https://spdx.org/licenses/), their abbreviations and links to full text
>  * Create a `LICENSE` file that points to the `LICENSES` folder
>    * You can use [this projects license](../../LICENSE.md) as a template

## Writing a Contribution Guide
* A contribution guide manifests your process on how to make changes in your project
* It outlines the workflow, quality goals, code style,…
  * Keep your contribution guide manageable. If it is too long, nobody will read it.
  * If you decide on a standardized coding style, link to the reference document
  * See also: [How to write a contribution guide]
* The contribution guide usually resides in a file called `CONTRIBUTING`. File formats may vary.

> ### Do Together
> * Add a Contribution Guide
> * An example contribution guide template is contained in [the snippets]($2004198)

## Adding Members to the Project
* Other people will want to join in on the project, even if they are not part of the initial group
  * These are called _Contributors_
  * They can be added as _project members_ via the project settings
  * When adding people to the projects pay attention to the rights they are given when doing so
* Membership may also be requested.

> ### Do Together
> * Instructor show: Add a Helper or Co-Instructor as project member
> * Instructor show: Request access to a Co-Instructors project
>   * Make sure the Instructor and Co-Instructor are not already in the same group
> * (Have a Co-Instructor prepare groups of two or three to reduce confusion)
> * Have participants request membership for each other's project and add each other as a project member

---

> ## Wrap-Up
> * Groups can be used to share projects and increase the bus-factor
> * How to create a new repository and set it up with a README
> * Licensing is important and the [reuse specification] can offer guidance
> * A contribution guide helps to get people started with the project

[GitLab Permissions]: https://gitlab.hzdr.de/help/user/permissions
[Standard Readme]: https://github.com/RichardLitt/standard-readme
[Creative Commons Licenses]: https://creativecommons.org/about/cclicenses/
[How to write a contribution guide]: https://opensource.com/life/16/3/contributor-guidelines-template-and-tips
[reuse specification]: https://reuse.software/spec/
