<!---
SPDX-FileCopyrightText: 2020 HIFIS <hifis-info@hzdr.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# Navigating Files and Branches
> **Estimated time:** 30 minutes
>
> ## Learning Goals
> * Navigate the document structure
> * Create files and directories using the Web IDE
> * Creating and switching branches
>
> ### Note
> This episode's content overlaps with the skills used in the first episode, feel free to skip the parts that were already explained. 
---

## Navigating the Document Structure
* The project's default view already presents the file structure of the current project branch
  * The displayed branch is _master_ by default
  * Otherwise `Repository` → `Files` switches to the file view
* If a folder contains a file named `README.md` it is automatically displayed under the file view
* Some files (e.g. Markdown files) automatically are displayed in a rendered view when selected
  * There is a button to switch to the source code view
* When viewing a directory, files and subdirectories can be added directly
  * These actions will result in a commit to the repository

## Managing Branches
* In the file view, branches can be switched in the drop-down menu at the top
* There is also `Repository` → `Branches` where all branches are listed and new ones can be created
* Further, in most views the most recent commits are linked
  * Clicking on the commit hashes displays the changes made in the commit

> ## Do Together
> * Create an new folder `source`
> * In it, add the `wator.py` file [from the snippets](https://gitlab.com/hifis/hifis-workshops/project-management-with-gitlab/-/snippets/2015297)
>  * How many ways are there to achieve that?
> * Don't forget to commit the changes
> * Create a new branch `test-different-parameters`
> * Change the parameters at the top of the `wator.py` file
> * Commit the changes
> * Use `Repository` → `Compare` to find the difference between these branches
> * Use the `blame` button to find out who changed the parameters and when

## Accessing the Repository with Git from the Command Line
* Each repository provides a _clone_-URL
  * This can be used in conjunction with `git clone <URL>` to obtain a local copy
  * Cloning via `ssh` requires to associate an _SSH Key_ with the account. See [GitLab SSH Documentation]
  * Use `https` for the sake of this workshop (easier, but less secure and comfortable)
* After cloning, the repository can be used offline as well
* Synchronize with the GitLab version via `git pull`/`git push`
  * Remember to _pull_ (or at least _fetch_) first, especially when working in a team

---
> ## Wrap-Up
> * The WebUI allows to navigate and edit files and directories in the repository
> * Any change made end up as a commit
> * The _Blame_-view allows to quickly identify who edited what and when
> * GitLab Repositories can be used from the command line as well

[GitLab SSH Documentation](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)
