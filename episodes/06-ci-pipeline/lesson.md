<!---
SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf <hifis-info@hzdr.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# The CI Pipeline
> **Estimated time:** ?? minutes
>
> ## Learning Goals
> * What is _Continuous Integration_?
> * How to set up a CI pipeline?

---

## Getting to Know CI

* CI = _Continuous Integration_
* Goal: Every commit made to a branch should automatically be checked
* This takes workload off the reviewers and automates mundane tasks required to ensure quality
  * Try to create and run programs from the comitted code
  * Automatically run software tests
  * Check for compliance with style guidelines
  * Check for license headers
  * … (the possibilities are endless)

* GitLab has intergrated CI features
* The core concept is called a pipeline

## Structure of a CI Pipeline

* [Pipeline Documentation]
* A _pipeline_ is built from _stages_
  * Stages are executed in the order they are defined, one after the other
* Each _stage_ consists of multiple _jobs_
  * All jobs of a stage are executed in parallel
  * Only when all jobs of a stage are finished, the stage is completed
  * Jobs are in essence shell scripts which can either be successful or result in an error
* A pipeline is considered successful if all jobs in all stages are successful

![https://docs.gitlab.com/ee/ci/pipelines/img/pipelines_index_v13_0.png]


## How to set up CI with GitLab?
> * Each pipeline is tailored to the project it is concerned with
> * Setting up pipelines is not trivial!

* A pipeline is added by creating a special file in the project
  * GitLab will handle the required background work on its own
* Put the file `.gitlab-ci.yml` into the project's root folder
* Within this file, the structure of the pipeline and its contitions and actions are described
  * [.gitlab-ci reference]

### Select an Image
The image emulates the environment in which your pipeline runs.
* Think of it as an imaginary computer with specific software installed
* Allows to run the jobs on a fresh setup without interferring configuration
  * Reduces _works-on-my-machine_ issues
* Select an image from [DockerHub]

### Define the Stages
* By default there are three stages:
  * `build`
  * `test`, jobs without a stage specified go in here
  * `deploy`

### Write the Job Scripts
* Job scripts are basically a list of shell commands that get executed for each job

> ## Do Together
>
> * Create a folder `paper` in your repository
> * In it, add the file [paper.tex](https://gitlab.com/hifis/hifis-workshops/project-management-with-gitlab/-/snippets/2009680)
> * Now create the [`gitlab-ci.yml`](https://gitlab.com/hifis/hifis-workshops/project-management-with-gitlab/-/snippets/2009678) in the project's root folder

## Inspecting the Pipeline
* In the menu option `CI/CD`
  * Either as the pipeline view or the job view
* If the pipeline succeeded and there are artifacts they can be viewed in the job details (right side bar)

---

> ## Wrap-Up
> * CI automates repetitive tasks and is great for quality control
> * Setting up a CI pipeline is easy to learn and hard to master


[Pipeline Documentation]: https://docs.gitlab.com/ee/ci/pipelines/
[DockerHub]: https://hub.docker.com/search?image_filter=official&type=image
[.gitlab-ci reference]: https://docs.gitlab.com/ee/ci/yaml/README.html
